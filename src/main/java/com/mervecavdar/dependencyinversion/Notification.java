package com.mervecavdar.dependencyinversion;

public class Notification {

    private final Email email = new Email();

    private final SMS sms = new SMS();

    public void sender() {
        email.sendEmail();
        sms.sendSMS();
    }

}
