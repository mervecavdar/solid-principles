package com.mervecavdar.dependencyinversion.solution;

public class SMS implements Message {

    @Override
    public void sendMessage() {
        sendSMS();
    }

    private void sendSMS() {
        //send sms
    }

}
