package com.mervecavdar.dependencyinversion.solution;

import java.util.List;

public class Notification {

    private final List<Message> messageList;

    public Notification(List<Message> messageList) {
        this.messageList = messageList;
    }

    public void sender() {
        for (Message message : messageList) {
            message.sendMessage();
        }
    }

}
