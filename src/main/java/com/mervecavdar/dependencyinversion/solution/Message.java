package com.mervecavdar.dependencyinversion.solution;

public interface Message {

    void sendMessage();

}
