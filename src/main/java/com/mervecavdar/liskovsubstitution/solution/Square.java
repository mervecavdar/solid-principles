package com.mervecavdar.liskovsubstitution.solution;

public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);
    }

}
