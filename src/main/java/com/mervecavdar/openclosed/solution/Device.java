package com.mervecavdar.openclosed.solution;

public class Device extends Product {

    @Override
    public double calculateTaxIncludedPrice() {
        return getBasePrice() * 1.18;
    }

}
