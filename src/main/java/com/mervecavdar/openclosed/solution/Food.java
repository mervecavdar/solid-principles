package com.mervecavdar.openclosed.solution;

public class Food extends Product {

    @Override
    public double calculateTaxIncludedPrice() {
        return getBasePrice() * 0.08;
    }

}
