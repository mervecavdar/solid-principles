package com.mervecavdar.interfacesegregation;

public interface Conversion {

    public void intToDouble();

    public void intToChar();

    public void charToString();

}
