package com.mervecavdar.interfacesegregation.solution;

public class DataTypeConversion implements ConvertIntToDouble, ConvertIntToChar, ConvertCharToString {

    @Override
    public void intToDouble() {
        //conversion logic
    }

    @Override
    public void intToChar() {
        //conversion logic
    }

    @Override
    public void charToString() {
        //conversion logic
    }

}
