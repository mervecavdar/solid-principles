package com.mervecavdar.singleresponsibility;

public class Employee {

    public void addEmployee() {
        //functionality of the method
    }

    public void printDetail() {
        //functionality of the method
    }

    public void calculateSalary() {
        //functionality of the method
    }

}
